package controllers;

import io.ebean.Ebean;
import models.Author;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */

public class AuthorController extends Controller {


    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index(Http.Request request) {
        List<Author> auhtors = Ebean.find(Author.class).findList();
        return ok(Json.toJson(auhtors));
    }

    public Result show(Http.Request request,String auhtor_id) {
        Author auhtor = Ebean.find(Author.class).where().eq("id",auhtor_id).findOne();
        return ok(Json.toJson(auhtor));
    }

    public Result delete(Http.Request request,String auhtor_id) {
        Author auhtor = Ebean.find(Author.class).where().eq("id",auhtor_id).findOne();
        auhtor.delete();
        return ok(Json.toJson(auhtor));
    }

    public Result post(Http.Request request) {
        Author check = Ebean.find(Author.class).where().eq("email",request.body().asFormUrlEncoded().get("email")[0]).findOne();
        if (check == null){
            Author author = new Author(request.body().asFormUrlEncoded().get("firstName")[0],request.body().asFormUrlEncoded().get("lastName")[0],request.body().asFormUrlEncoded().get("email")[0],request.body().asFormUrlEncoded().get("password")[0],null);
            author.insert();
            return ok(views.html.login.render());
        }
        return ok(views.html.register.render());
    }

}

package controllers;

import io.ebean.Ebean;
import models.Author;
import models.Book;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */


public class BookController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index(Http.Request request) {
        if (request.session().data().size() != 0){
            String email = request.session().get("connected").get();
            Author author = Ebean.find(Author.class).where().eq("email",email).findOne();
            return ok(views.html.books.render(author.getBooks()));
        }
        return redirect("/");
    }

    public Result post(Http.Request request) {
        if (request.session().data().size() != 0){
            String email = request.session().get("connected").get();
            Author author = Ebean.find(Author.class).where().eq("email",email).findOne();
            Book book = new Book(request.body().asFormUrlEncoded().get("title")[0],request.body().asFormUrlEncoded().get("price")[0],author);
            book.insert();
            return ok(views.html.books.render(author.getBooks()));
        }
        return redirect("/");
    }

    public Result show(Http.Request request,String book_id) {
        if (request.session().data().size() != 0){
            String email = request.session().get("connected").get();
            Author author = Ebean.find(Author.class).where().eq("email",email).findOne();
            List<Book> books = author.getBooks().stream().filter(book1 -> book1.getId().toString().equals(book_id)).collect(Collectors.toList());
            if (books.size() > 0){
                return ok(views.html.bookshow.render(books.get(0)));
            }
            return ok(views.html.books.render(author.getBooks()));
        }
        return redirect("/");
    }

    public Result delete(Http.Request request,String book_id) {
        if (request.session().data().size() != 0){
            String email = request.session().get("connected").get();
            Author author = Ebean.find(Author.class).where().eq("email",email).findOne();
            List<Book> books = author.getBooks().stream().filter(book1 -> book1.getId().toString().equals(book_id)).collect(Collectors.toList());
            if (books.size() > 0){
                books.get(0).delete();
                return ok(views.html.books.render(author.getBooks().stream().filter(book1 -> !book1.getId().toString().equals(book_id)).collect(Collectors.toList())));
            }
            return ok(views.html.books.render(author.getBooks()));
        }
        return redirect("/");
    }

    public Result newBook(Http.Request request) {
        if (request.session().data().size() != 0){
            return ok(views.html.newbook.render());
        }
        return redirect("/");
    }
}

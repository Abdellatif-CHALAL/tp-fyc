package controllers;

import io.ebean.Ebean;
import models.Author;
import models.Book;
import play.mvc.*;

import java.util.List;


public class LoginController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    public Result loginForm(Http.Request request) {
        return ok(views.html.login.render());
    }

    public Result register(Http.Request request) {
        return ok(views.html.register.render());
    }

    public Result logout(Http.Request request) {
        List<Book> books =  Ebean.find(Book.class).where().findList();
        return ok(views.html.index.render(books)).removingFromSession(request, "connected");
    }

}

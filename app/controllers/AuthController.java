package controllers;

import io.ebean.Ebean;
import models.Author;
import play.libs.Json;
import play.mvc.*;


import javax.inject.Inject;


public class AuthController extends Controller {

    public Result token(Http.Request request) {
        String email = request.body().asFormUrlEncoded().get("email")[0];
        String password = request.body().asFormUrlEncoded().get("password")[0];
        Author author = Ebean.find(Author.class).where().eq("email",email).findOne();
        if (password.equals(author.getPassword())) {
            return redirect("/books").addingToSession(request, "connected", author.getEmail());
        } else {
            return forbidden("Wrong credentials");
        }
    }

    public Result json(Object object) {
        return ok(Json.toJson(object));
    }

}

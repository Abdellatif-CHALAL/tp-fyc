package models;


import io.ebean.Model;

import javax.persistence.*;

@Entity
public class Book extends Model {

    @Id
    @GeneratedValue
    private Integer id;
    private String title;
    @Column(nullable = false)
    private String price;
    @ManyToOne
    private Author author;

    public Book(String title, String price, Author author) {
        this.title = title;
        this.price = price;
        this.author = author;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
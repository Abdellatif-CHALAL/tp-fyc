# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table author (
  id                            integer auto_increment not null,
  first_name                    varchar(255),
  last_name                     varchar(255) not null,
  email                         varchar(255),
  password                      varchar(255),
  constraint pk_author primary key (id)
);

create table book (
  id                            integer auto_increment not null,
  title                         varchar(255),
  price                         varchar(255) not null,
  author_id                     integer,
  constraint pk_book primary key (id)
);

create index ix_book_author_id on book (author_id);
alter table book add constraint fk_book_author_id foreign key (author_id) references author (id) on delete restrict on update restrict;


# --- !Downs

alter table book drop foreign key fk_book_author_id;
drop index ix_book_author_id on book;

drop table if exists author;

drop table if exists book;


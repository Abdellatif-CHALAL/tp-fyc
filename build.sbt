name := """new-app"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val myProject = (project in file("."))
  .enablePlugins(PlayJava,PlayEbean)

scalaVersion := "2.13.6"

libraryDependencies ++= Seq(
  guice,
  javaJdbc,
  evolutions,
  "mysql" % "mysql-connector-java" % "8.0.16"
)

